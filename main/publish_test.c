#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdint.h>
#include <stddef.h>
#include "tcpip_adapter.h"
#include "protocol_examples_common.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"
#include "freertos/semphr.h"
#include "driver/gpio.h"
#include "esp_err.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_attr.h"
#include "esp_event_loop.h"
#include "esp_sleep.h"
#include "esp_spi_flash.h"
#include "esp32/rom/spi_flash.h"
#include "nvs_flash.h"
#include "driver/i2c.h"

#include "esp_sntp.h"

#include "lwip/err.h"
#include "lwip/sys.h"

#include "lwip/inet.h"
#include "lwip/ip4_addr.h"
#include "lwip/dns.h"
#include "ping/ping.h"
#include "esp_ping.h"

#include "lwip/sockets.h"
#include "lwip/netdb.h"

#include "esp_timer.h"
#include "sdkconfig.h"
#include "mqtt_client.h"
#include "esp_smartconfig.h"

#define MAX_APs 					10
//#define MSG_LEN						512
//#define MAX_HTTP_RECV_BUFFER 		512
//#define MAX_HTTP_OUTPUT_BUFFER 		2048

//SynologySmartConnect
//Life9@405
//JioFi2_196680
//Rar@Sandy
//Pravani 2nd Floor
//Ramakrishna
//JioFi_shantha
//Bms@1969
//#define EXAMPLE_ESP_WIFI_SSID		"LIFE9"
//#define EXAMPLE_ESP_WIFI_PASS		"Life9@034"
//#define EXAMPLE_ESP_WIFI_SSID1		"L9 abhi"
//#define EXAMPLE_ESP_WIFI_PASS1		"12345678"

/* FreeRTOS event group to signal when we are connected*/
static EventGroupHandle_t s_wifi_event_group;

/* The event group allows multiple bits for each event, but we only care about two events:
 * - we are connected to the AP with an IP
 * - we failed to connect after the maximum amount of retries */
#define WIFI_CONNECTED_BIT 			BIT0
#define WIFI_FAIL_BIT      			BIT1
#define ESPTOUCH_DONE_BIT      		BIT2
uint8_t wifi_smartconfig_status;
esp_timer_handle_t wifi_smartconfig_timer;
#define wifi_smartconfig_TIMER_COUNT	30000000

wifi_config_t wifi_config;
EventBits_t uxBits;
char ssid[33] = { "\0" };
char password[65] = { "\0" };

static EventGroupHandle_t mqtt_event_group;
const static int CONNECTED_BIT = BIT0;

static esp_mqtt_client_handle_t mqtt_client = NULL;
#define MQTT_DAILY_MAX_PAC_COUNT				50000
char *Mqtt_L9_ServerURL = "mqtt://3.7.202.112:1883";
uint8_t qos = 2;
uint8_t mqtt_retain = 1;
char Mqtt_Pub_Topic[15] = {"\0"};
char *Mqtt_Sub_Topic = "bcddc2de772e";

#if CONFIG_EXAMPLE_BROKER_CERTIFICATE_OVERRIDDEN == 1
static const uint8_t mqtt_eclipse_org_pem_start[]  = "-----BEGIN CERTIFICATE-----\n" CONFIG_EXAMPLE_BROKER_CERTIFICATE_OVERRIDE "\n-----END CERTIFICATE-----";
#else
extern const uint8_t mqtt_eclipse_org_pem_start[]   asm("_binary_mqtt_eclipse_org_pem_start");
#endif
//extern const uint8_t mqtt_eclipse_org_pem_end[]   asm("_binary_mqtt_eclipse_org_pem_end");

#define GPIO_EXPANDER_RESET_ENABLE	23
#define GPIO_OUTPUT_ESP_LED1_RED	32
#define GPIO_OUTPUT_ESP_LED2_AMBER	33
#define GPIO_OUTPUT_ESP_LED3_GREEN	25

//#define GPIO_INPUT_P_IR			4
//hand sensor
#define GPIO_INPUT_P_IR				4
//#define GPIO_INPUT_LL_IR			26
#define GPIO_INPUT_LL_IR			26
#define GPIO_INPUT_LF_IR			34
#define GPIO_INPUT_LOW_BATT			27
#define GPIO_EXPANDER_INT			35

#define GPIO_INPUT_PIN_SEL  		((1ULL<<GPIO_INPUT_P_IR) | (1ULL<<GPIO_INPUT_LL_IR) | (1ULL<<GPIO_INPUT_LF_IR) | (1ULL<<GPIO_EXPANDER_INT))
#define GPIO_OUTPUT_PIN_SEL  		((1ULL<<GPIO_OUTPUT_ESP_LED1_RED) | (1ULL<<GPIO_OUTPUT_ESP_LED2_AMBER) | (1ULL<<GPIO_OUTPUT_ESP_LED3_GREEN))

#define GPIO_INT_TASK_PRIORITY		10
#define GPIO_INT_STACK_DEPTH		2048
#define GPIO_INT_QUEUE_SIZE			10
#define HIGH						1
#define LOW							0
#define TRUE						1
#define FALSE						0
//#define Debug_Log 					0

#ifdef Debug_Log
static const char *TAG = "wifi station";
#endif

#define DEVICE_ID					0x1540ef
#define FLASH_SIZE					(8*1024*1024)
#define FLASH_BLOCK_SIZE			(64*1024)
//#define FLASH_SECTOR_SIZE			4096
#define FLASH_PAGE_SIZE				256
#define STATUS_MASK					0xffff
#define BASE_ADDR					0x7b0000
#define FLASH_SECTOR_SIZE			0x1000
#define SSID_PRIMARY_ADDR			BASE_ADDR
#define PASSWORD_PRIMARY_ADDR		(SSID_PRIMARY_ADDR + FLASH_SECTOR_SIZE)

#define STA_ID_FLASH_RBUFF_LENGHT					16
#define INSTALLTION_DATE_TIME_FLASH_RBUFF_LENGTH	20
#define SERVER_URL_FLASH_RBUFF_LENGTH				28
#define IOT_NONIOT_MODE_FLASH_RBUFF_LENGTH			4
#define TIME_ZONE_FLASH_RBUFF_LENGTH				12
#define SSID_PRIMARY_RBUFF_LENGTH					20
#define PASSWORD_PRIMARY_RBUFF_LENGTH				20
#define SSID_SECONDARY_RBUFF_LENGTH					20
#define PASSWORD_SECONDARY_RBUFF_LENGTH				20
#define DESPENSE_VOLUME_RBUFF_LENGTH				4
#define MAC_ID_RBUFF_LENGTH							16

#define ESP_INTR_FLAG_DEFAULT 		0

#define PING_COUNT					6

#define SYSTEM_HEALTH_STATUS_TIME			3600000000

#define RED_LED_SHORT_ON					100000
#define RED_LED_SHORT_OFF					400000
#define RED_LED_BLINK_ON					100000
#define RED_LED_BLINK_OFF					100000
#define RED_LED_LONG_ON						300000
#define RED_LED_LONG_OFF					200000
uint32_t RED_LED_TIMER_COUNT;
static bool RED_LED_STATUS;
esp_timer_handle_t power_init_timer;

#define AMBER_LED_SHORT_ON					100000
#define AMBER_LED_SHORT_OFF					400000
#define AMBER_LED_BLINK_ON					30000
#define AMBER_LED_BLINK_OFF					30000
#define BUZZER_FLUID_LOW_LONG_ON			300000
#define BUZZER_FLUID_LOW_LONG_OFF			200000
#define FLUID_LOW_BUZZER_DELAY				2000000
uint32_t AMBER_LED_TIMER_COUNT;
uint32_t FLUID_LOW_TIMER_COUNT;
static bool AMBER_LED_STATUS;
uint8_t FLUID_LOW_STATUS;
esp_timer_handle_t fluid_low_buzzer_delay_timer;
esp_timer_handle_t fluid_led_blink_timer;
esp_timer_handle_t fluid_low_long_timer;
esp_timer_handle_t fluid_dispense_motor_timer;

#define GREEN_LED_SHORT_ON					100000
#define GREEN_LED_SHORT_OFF					400000
#define GREEN_LED_BLINK_ON					10000
#define GREEN_LED_BLINK_OFF					30000
#define GREEN_LED_LONG_ON					300000
#define GREEN_LED_LONG_OFF					200000
uint32_t GREEN_LED_TIMER_COUNT;
static bool GREEN_LED_STATUS;
esp_timer_handle_t wifi_led_short_timer;
esp_timer_handle_t wifi_led_blink_timer;

#define BUZZER_FLUID_FULL_ON				600000
#define BUZZER_FLUID_FULL_OFF				200000
#define HIGH_LEVEL_DETECT_BUZZER_COUNT		10
uint8_t High_level_Buzzer_Count;
#define LOW_LEVEL_DETECT_BUZZER_COUNT		10
uint8_t Low_level_Buzzer_Count;
uint32_t FLUID_FULL_TIMER_COUNT;
uint8_t fluid_low_buzzer_delay_timer_status;
uint8_t FLUID_FULL_STATUS;
esp_timer_handle_t fluid_full_long_timer;

#define IoT_MODE					192
#define NON_IoT_MODE				128
uint8_t SW_IoT_NonIoT_Status;


// Definitions for i2c
#define I2C_MASTER_SCL_IO   		22
#define I2C_MASTER_SDA_IO   		21
#define I2C_MASTER_NUM  			I2C_NUM_0
#define I2C_MASTER_TX_BUF_DISABLE   0
#define I2C_MASTER_RX_BUF_DISABLE   0
#define I2C_MASTER_FREQ_HZ  		400000

#define GPIO_EXPANDER_DEV_ADDR		0x21
#define EXPANDER_CONFIG_ADDR		0x03
#define EXPANDER_OUT_PORT_ADDR		0x01
#define EXPANDER_IN_PORT_ADDR		0x00
#define EXPANDER_CONFIG_DATA		0xD0
#define EXPANDER_BUZZER_DATA		0x08
#define EXPANDER_MOTOR_DATA			0x05
#define EXPANDER_NULL_DATA			0x00

#define WRITE_ADDR   				0x00
#define READ_ADDR    				0x01

#define EEPROM_PAGE_SIZE			32

#define MSG_HEADER_LEN				30
#define FL_LVL_MSG_LEN  			1
#define FL_LVL_MSG_TYPE				0x44
#define FL_DISP_MSG_LEN  			3
#define FL_DISP_MSG_TYPE			0x43
#define CONFIG_ACK_MSG_LEN			8
#define POST_MSG_LEN     			10
#define POST_MSG_TYPE				0x00
#define BATTERY_NOT_PRESENT			0xFF
#define BATTERY_PRESENT				0x01
#define COLD_START					0x01
#define SERVER_RESTART				0x02
#define APP_RESTART					0x03

ip_addr_t ip_Addr;

ip4_addr_t ip;
ip4_addr_t gw;
ip4_addr_t msk;
uint32_t ping_count 				= 25;  //how many pings per report
uint32_t ping_timeout 				= 1000; //mS till we consider it timed out
uint32_t ping_delay 				= 500; //mS between pings

static xQueueHandle gpio_evt_queue 	= NULL;

typedef struct flash_config_data{
	char devise_serial_no[16];
	char installation_date_time[20];
	char server_url[28];
	uint32_t iot_noniot_mode;
	uint32_t dispense_volume;
	char time_zone[12];
	char ssid_primary[20];
	char password_primary[20];
	char ssid_secondary[20];
	char password_secondary[25];
	uint8_t sta_id[16];
}flash_config_data;
static flash_config_data flash_data;

#define STA_ID_PADDING_START		6
#define STA_ID_PADDING_END			15
typedef struct Msg_header{
	uint16_t pac_id					: 16;
	uint8_t  sta_id[15];
	uint8_t  msg_type				: 8;
	uint8_t  msg_length				: 8;
	struct time_stamp{
		uint8_t		hour			: 8;
		uint8_t		min				: 8;
		uint8_t		sec				: 8;
		uint8_t		day				: 8;
		uint8_t		month			: 8;
		uint16_t	year			: 16;
	}DateTime;
}Msg_header;
static Msg_header header;

typedef struct Config_data_sub{
	uint dispense_volume;
	uint rfid_flag;
	uint version;
	char liquid_type[2];
	uint msg_type;
	uint msg_length;
	char time[20];
}Config_data_sub;
static Config_data_sub config;

typedef struct Post_payload{
	uint8_t	boot_reason				: 8;
	uint8_t	heart_beat				: 8;
	uint8_t battery_detect			: 8;
}Post_payload;
static Post_payload post;

#define RSSI_AVG_COUNT				10
#define BATTERY_LOW					0x20
#define BATTERY_FULL				0x64
#define MAINS_POWERED				0x01
#define BATTERY_POWERED				0x02
typedef struct System_status{
	uint8_t	rssi					: 8;
	uint8_t battery					: 8;
	uint8_t power_status			: 8;
	uint8_t liquid_type				: 8;
	uint8_t rfid					: 8;
}System_status;
static System_status status;

typedef struct Ping_status{
	uint16_t ping_max				: 16;
	uint16_t ping_min				: 16;
	uint16_t ping_mean				: 16;
	uint8_t ping_payload			: 8;
	uint8_t ping_tx_cnt				: 8;
	uint8_t ping_rx_cnt				: 8;
	uint8_t ping_lost_cnt			: 8;
}Ping_status;
static struct Ping_status ping;

typedef struct Fluid_dispense{
	uint8_t dispense_volume			: 8;
	uint16_t dispense_count			: 16;
}Fluid_dispense;
static Fluid_dispense dispense;
#define DEFAULT_DISPENSE_VOLUME		0x02
static uint32_t CONF_DELAY = 200000;
static uint8_t fluid_dispence_status = 0;

typedef struct Fluid_level{
	char level;
}Fluid_level;
static Fluid_level FluidLevel;
#define FLUID_LEVEL_LOW					0x0A
#define FLUID_LEVEL_BELOW_CENTER		0x14
#define FLUID_LEVEL_CENTER				0x32
#define FLUID_LEVEL_ABOVE_CENTER		0x50
#define FLUID_LEVEL_HIGH				0x64

uint8_t conf_fluid_dispense_ack;

#define SMART_CONFIG_INPROGRESS			0
#define SMART_CONFIG_TIME_OVER			1
#define SMART_CONFIG_STACK_DEPTH		4096
#define SMART_CONFIG_TASK_PRIORITY		3
TaskHandle_t wifi_smart_config_task;

#ifdef CONFIG_SNTP_TIME_SYNC_METHOD_CUSTOM
void sntp_sync_time(struct timeval *tv)
{
   settimeofday(tv, NULL);
   //ESP_LOGI(TAG, "Time is synchronized from custom code");
   sntp_set_sync_status(SNTP_SYNC_STATUS_COMPLETED);
}
#endif

void flash_write(char *wbuf,int base_addr){
	spi_flash_erase_sector(base_addr / SPI_FLASH_SEC_SIZE);
	#ifdef Debug_Log
    printf("writing at %x  %s\n\n\n", base_addr, wbuf);
	#endif
    spi_flash_write(base_addr, wbuf, strlen(wbuf)+1);
}

void flash_read(char *rbuf,int base_addr,int rlength){
	#ifdef Debug_Log
    printf("reading at %x  ", base_addr);
	#endif
    spi_flash_read(base_addr, rbuf, rlength);
	#ifdef Debug_Log
    printf("g_rbuf: %s\n\n\n",rbuf);
	#endif
}

static void wifi_smartconfig_callback(void* arg)
{
	#ifdef Debug_Log
	printf("...wifi_smartconfig_callback...\n");
	#endif
    ESP_ERROR_CHECK(esp_timer_stop(wifi_smartconfig_timer));
	esp_smartconfig_stop();
	wifi_smartconfig_status = SMART_CONFIG_TIME_OVER;
    vTaskDelete(wifi_smart_config_task);
}

static void smartconfig_task(void * parm)
{
//    EventBits_t uxBits;
    ESP_ERROR_CHECK( esp_smartconfig_set_type(SC_TYPE_ESPTOUCH) );
    smartconfig_start_config_t cfg = SMARTCONFIG_START_CONFIG_DEFAULT();
    ESP_ERROR_CHECK( esp_smartconfig_start(&cfg) );

    while (wifi_smartconfig_status == SMART_CONFIG_INPROGRESS) {
        uxBits = xEventGroupWaitBits(s_wifi_event_group, WIFI_CONNECTED_BIT | ESPTOUCH_DONE_BIT, true, false, portMAX_DELAY);

        if (uxBits & WIFI_CONNECTED_BIT) {
//        	wifi_smartconfig_status = 0;
			#ifdef Debug_Log
        	ESP_LOGI(TAG, "connected to ap SSID:%s password:%s",
                     flash_data.ssid_primary, flash_data.password_primary);
			#endif
        	esp_wifi_get_mac(WIFI_IF_STA,header.sta_id);
        	sprintf(Mqtt_Pub_Topic,"%.2x%.2x%.2x%.2x%.2x%.2x",header.sta_id[0],header.sta_id[1],header.sta_id[2],header.sta_id[3],header.sta_id[4],header.sta_id[5]);
			#ifdef Debug_Log
        	printf("Mqtt_Pub_Topic:%s     \n",Mqtt_Pub_Topic);
			#endif
        	//STATION ID PADDING AS PER MQTT DOCUMENT
        	for(uint8_t i=STA_ID_PADDING_START;i<STA_ID_PADDING_END;i++){
        	    header.sta_id[i] = 0xFF;
        	}
        	ESP_ERROR_CHECK(esp_timer_stop(wifi_smartconfig_timer));
        }
        if(uxBits & ESPTOUCH_DONE_BIT) {
//            wifi_smartconfig_status = 0;
			#ifdef Debug_Log
            ESP_LOGI(TAG, "smartconfig over");
			#endif
            esp_smartconfig_stop();
            vTaskDelete(wifi_smart_config_task);
        }
    }
}

static void event_handler(void* arg, esp_event_base_t event_base,
                                int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
    	esp_wifi_connect();
        xTaskCreate(smartconfig_task, "smartconfig_task", SMART_CONFIG_STACK_DEPTH, NULL, SMART_CONFIG_TASK_PRIORITY, &wifi_smart_config_task);
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
    	esp_wifi_connect();
    	xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
		#ifdef Debug_Log
    	ESP_LOGI(TAG,"connect to the AP fail");
		#endif
    } else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    } else if (event_base == SC_EVENT && event_id == SC_EVENT_SCAN_DONE) {
		#ifdef Debug_Log
        ESP_LOGI(TAG, "Scan done");
		#endif
    } else if (event_base == SC_EVENT && event_id == SC_EVENT_FOUND_CHANNEL) {
		#ifdef Debug_Log
        ESP_LOGI(TAG, "Found channel");
		#endif
    } else if (event_base == SC_EVENT && event_id == SC_EVENT_GOT_SSID_PSWD) {
		#ifdef Debug_Log
        ESP_LOGI(TAG, "Got SSID and password");
		#endif

        smartconfig_event_got_ssid_pswd_t *evt = (smartconfig_event_got_ssid_pswd_t *)event_data;

        bzero(&wifi_config, sizeof(wifi_config_t));
        memcpy(flash_data.ssid_primary, evt->ssid, sizeof(evt->ssid));
        memcpy(flash_data.password_primary, evt->password, sizeof(evt->password));
        memcpy(wifi_config.sta.ssid, evt->ssid, sizeof(wifi_config.sta.ssid));
        memcpy(wifi_config.sta.password, evt->password, sizeof(wifi_config.sta.password));

        wifi_config.sta.bssid_set = evt->bssid_set;
        if (wifi_config.sta.bssid_set == true) {
            memcpy(wifi_config.sta.bssid, evt->bssid, sizeof(wifi_config.sta.bssid));
        }

		#ifdef Debug_Log
        ESP_LOGI(TAG, "SSID:%s", flash_data.ssid_primary);
        ESP_LOGI(TAG, "PASSWORD:%s", flash_data.password_primary);
		#endif

    } else if (event_base == SC_EVENT && event_id == SC_EVENT_SEND_ACK_DONE) {
        xEventGroupSetBits(s_wifi_event_group, ESPTOUCH_DONE_BIT);
    }
}

static void IRAM_ATTR gpio_isr_handler(void* arg)
{
    uint32_t gpio_num = (uint32_t) arg;
    xQueueSendFromISR(gpio_evt_queue, &gpio_num, NULL);
}

static void initialise_wifi(void)
{
	EventBits_t bits;
	#ifdef Debug_Log
	char f_read[100] = {"\0"};
	#endif

    tcpip_adapter_init();
    s_wifi_event_group = xEventGroupCreate();
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK( esp_wifi_init(&cfg) );

    ESP_ERROR_CHECK( esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL) );
    ESP_ERROR_CHECK( esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler, NULL) );
    ESP_ERROR_CHECK( esp_event_handler_register(SC_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL) );

    ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK( esp_wifi_start() );
    ESP_ERROR_CHECK( esp_wifi_disconnect() );

    const esp_timer_create_args_t wifi_smartconfig = {
                .callback = &wifi_smartconfig_callback,
                /* name is optional, but may help identify the timer when debugging */
                .name = "wifi_smartconfig"
    };
    /* The timer has been created but is not running yet */
    ESP_ERROR_CHECK(esp_timer_create(&wifi_smartconfig, &wifi_smartconfig_timer));
    /* Start the timers */
    ESP_ERROR_CHECK(esp_timer_start_periodic(wifi_smartconfig_timer, wifi_smartconfig_TIMER_COUNT));

    while(wifi_smartconfig_status == SMART_CONFIG_INPROGRESS){
    	if(strcmp(flash_data.ssid_primary,"\0") != FALSE){
    		flash_write(flash_data.ssid_primary, SSID_PRIMARY_ADDR);
			#ifdef Debug_Log
    		memcpy(f_read, "\0", strlen(f_read));
    		flash_read(f_read, SSID_PRIMARY_ADDR, SSID_PRIMARY_RBUFF_LENGTH);
    		printf("smart config flash ssid_primary data:%s\n",f_read);
			#endif
    		flash_write(flash_data.password_primary, PASSWORD_PRIMARY_ADDR);
			#ifdef Debug_Log
    		memcpy(f_read, "\0", strlen(f_read));
    		flash_read(f_read, PASSWORD_PRIMARY_ADDR, PASSWORD_PRIMARY_RBUFF_LENGTH);
    		printf("smart config flash password_primary data:%s\n",f_read);
			#endif
//    		ESP_ERROR_CHECK( esp_wifi_disconnect() );
    		ESP_ERROR_CHECK( esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
    	    ESP_ERROR_CHECK( esp_wifi_connect() );
    		break;
    	}
    	vTaskDelay(500/portTICK_PERIOD_MS);
    }
    //EventBits_t Bits;
    if(strcmp(flash_data.ssid_primary,"\0") == FALSE){
    	flash_read(flash_data.ssid_primary, SSID_PRIMARY_ADDR, SSID_PRIMARY_RBUFF_LENGTH);
    	vTaskDelay(50/portTICK_PERIOD_MS);
    	flash_read(flash_data.password_primary, PASSWORD_PRIMARY_ADDR, PASSWORD_PRIMARY_RBUFF_LENGTH);
		#ifdef Debug_Log
    	printf("flash ssid data: %s    flash password data: %s\n",flash_data.ssid_primary,flash_data.password_primary);
		#endif
        bzero(&wifi_config, sizeof(wifi_config_t));
        memcpy(wifi_config.sta.ssid, flash_data.ssid_primary, sizeof(wifi_config.sta.ssid));
        memcpy(wifi_config.sta.password, flash_data.password_primary, sizeof(wifi_config.sta.password));

//		ESP_ERROR_CHECK( esp_wifi_disconnect() );
		ESP_ERROR_CHECK( esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
	    ESP_ERROR_CHECK( esp_wifi_connect() );
	    vTaskDelay(500/portTICK_PERIOD_MS);
    }
    if(wifi_smartconfig_status == SMART_CONFIG_TIME_OVER){
		bits = xEventGroupWaitBits(s_wifi_event_group,
				WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
				pdFALSE,
				pdFALSE,
				portMAX_DELAY);
		if (bits & WIFI_CONNECTED_BIT) {
			esp_wifi_get_mac(WIFI_IF_STA,header.sta_id);
			sprintf(Mqtt_Pub_Topic,"%.2x%.2x%.2x%.2x%.2x%.2x",header.sta_id[0],header.sta_id[1],header.sta_id[2],header.sta_id[3],header.sta_id[4],header.sta_id[5]);
			#ifdef Debug_Log
			printf("Mqtt_Pub_Topic:%s     \n",Mqtt_Pub_Topic);
			#endif
			//STATION ID PADDING AS PER MQTT DOCUMENT
        	for(uint8_t i=STA_ID_PADDING_START;i<STA_ID_PADDING_END;i++){
        	    header.sta_id[i] = 0xFF;
        	}
			#ifdef Debug_Log
			printf("connected to AP\n");
			#endif
		} else if (bits & WIFI_FAIL_BIT) {
			SW_IoT_NonIoT_Status = NON_IoT_MODE;
			#ifdef Debug_Log
			printf("non iot....\n");
			printf("Failed to connect with wifi\n");
			#endif
		}
    }
}

/*esp_err_t eeprom_write(uint8_t deviceaddress, uint16_t eeaddress, uint8_t* data, size_t size) {
	int i2c_master_port = I2C_MASTER_NUM;
    esp_err_t ret = ESP_OK;
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (deviceaddress << 1) | WRITE_ADDR, 1);
    i2c_master_write_byte(cmd, eeaddress>>8, 1);
    i2c_master_write_byte(cmd, eeaddress&0xFF, 1);

    int bytes_remaining = size;
    int current_address = eeaddress;
    int first_write_size = ((EEPROM_PAGE_SIZE-1) - eeaddress % (EEPROM_PAGE_SIZE-1))+1;
    if (eeaddress % (EEPROM_PAGE_SIZE-1) == 0 && eeaddress != 0) first_write_size = 1;
    if (bytes_remaining <= first_write_size) {
        i2c_master_write(cmd, data, bytes_remaining, 1);
    } else {
        i2c_master_write(cmd, data, first_write_size, 1);
        bytes_remaining -= first_write_size;
        current_address += first_write_size;
        i2c_master_stop(cmd);
        ret = i2c_master_cmd_begin(i2c_master_port, cmd, 1000/portTICK_PERIOD_MS);
        i2c_cmd_link_delete(cmd);
        if (ret != ESP_OK) return ret;
        while (bytes_remaining > 0) {
            cmd = i2c_cmd_link_create();

            // 2ms delay period to allow EEPROM to write the page
            // buffer to memory.
            vTaskDelay(20/portTICK_PERIOD_MS);

            i2c_master_start(cmd);
            i2c_master_write_byte(cmd, (deviceaddress << 1) | WRITE_ADDR, 1);
            i2c_master_write_byte(cmd, current_address>>8, 1);
            i2c_master_write_byte(cmd, current_address&0xFF, 1);
            if (bytes_remaining <= EEPROM_PAGE_SIZE) {
                i2c_master_write(cmd, data+(size-bytes_remaining), bytes_remaining, 1);
                bytes_remaining = 0;
            } else {
                i2c_master_write(cmd, data+(size-bytes_remaining), EEPROM_PAGE_SIZE, 1);
                bytes_remaining -= EEPROM_PAGE_SIZE;
                current_address += EEPROM_PAGE_SIZE;
            }
            i2c_master_stop(cmd);
            ret = i2c_master_cmd_begin(i2c_master_port, cmd, 1000/portTICK_PERIOD_MS);
            i2c_cmd_link_delete(cmd);
            if (ret != ESP_OK) return ret;
        }
    }

    return ret;
}

esp_err_t eeprom_read(uint8_t deviceaddress, uint16_t eeaddress, uint8_t* data, size_t size) {
	int i2c_master_port = I2C_MASTER_NUM;
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (deviceaddress<<1)|WRITE_ADDR, 1);
    i2c_master_write_byte(cmd, eeaddress<<8, 1);
    i2c_master_write_byte(cmd, eeaddress&0xFF, 1);
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (deviceaddress<<1)|READ_ADDR, 1);

    if (size > 1) {
        i2c_master_read(cmd, data, size-1, 0);
    }
    i2c_master_read_byte(cmd, data+size-1, 1);
    i2c_master_stop(cmd);
    esp_err_t ret = i2c_master_cmd_begin(i2c_master_port, cmd, 1000/portTICK_PERIOD_MS);
    i2c_cmd_link_delete(cmd);
    return ret;
}*/

void gpio_expander_write_byte(uint8_t deviceaddress, uint8_t eeaddress, uint8_t byte) {
	int i2c_master_port = I2C_MASTER_NUM;
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, (deviceaddress<<1)|WRITE_ADDR, 1);
	i2c_master_write_byte(cmd, eeaddress, 1);
	i2c_master_write_byte(cmd, byte, 1);
	i2c_master_stop(cmd);
	esp_err_t ret = i2c_master_cmd_begin(i2c_master_port, cmd, 1000 / portTICK_RATE_MS);
	i2c_cmd_link_delete(cmd);

    if (ret == ESP_OK) {
		#ifdef Debug_Log
        printf("Write OK");
		#endif
	}else if (ret == ESP_ERR_TIMEOUT) {
		#ifdef Debug_Log
        printf("Bus is busy");
		#endif
    } else {
		#ifdef Debug_Log
        printf("Write Failed");
		#endif
    }

//    i2c_driver_delete(i2c_master_port);
}

uint8_t gpio_expander_read_byte(uint8_t deviceaddress, uint8_t eeaddress) {
	int i2c_master_port = I2C_MASTER_NUM;
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (deviceaddress<<1)|WRITE_ADDR, 1);
    i2c_master_write_byte(cmd, eeaddress, 1);
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (deviceaddress<<1)|READ_ADDR, 1);

    uint8_t data;
    i2c_master_read_byte(cmd, &data, 1);
    i2c_master_stop(cmd);
    i2c_master_cmd_begin(i2c_master_port, cmd, 1000/portTICK_PERIOD_MS);
    i2c_cmd_link_delete(cmd);
    return data;
}

void init_i2c_master() {
    int i2c_master_port = I2C_MASTER_NUM;
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_MASTER_SDA_IO;
    conf.scl_io_num = I2C_MASTER_SCL_IO;
    conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    conf.master.clk_speed = I2C_MASTER_FREQ_HZ;
    i2c_param_config(i2c_master_port, &conf);
    i2c_driver_install(i2c_master_port, conf.mode, I2C_MASTER_RX_BUF_DISABLE, I2C_MASTER_TX_BUF_DISABLE, 0);
    gpio_expander_write_byte(GPIO_EXPANDER_DEV_ADDR, EXPANDER_OUT_PORT_ADDR, EXPANDER_NULL_DATA);
    vTaskDelay(50 / portTICK_RATE_MS);
    gpio_expander_write_byte(GPIO_EXPANDER_DEV_ADDR, EXPANDER_CONFIG_ADDR, EXPANDER_CONFIG_DATA);
    vTaskDelay(50 / portTICK_RATE_MS);
    SW_IoT_NonIoT_Status=gpio_expander_read_byte(GPIO_EXPANDER_DEV_ADDR, EXPANDER_IN_PORT_ADDR);
	#ifdef Debug_Log
    printf("\n\nin port=%d\n\n",SW_IoT_NonIoT_Status);
	#endif
    vTaskDelay(50 / portTICK_RATE_MS);
}

void time_sync_notification_cb(struct timeval *tv)
{
    //ESP_LOGI(TAG, "Notification of a time synchronization event");
}

static void initialize_sntp(void)
{
    //ESP_LOGI(TAG, "Initializing SNTP");
    sntp_setoperatingmode(SNTP_OPMODE_POLL);
    sntp_setservername(0, "pool.ntp.org");
    sntp_set_time_sync_notification_cb(time_sync_notification_cb);
	#ifdef CONFIG_SNTP_TIME_SYNC_METHOD_SMOOTH
    sntp_set_sync_mode(SNTP_SYNC_MODE_SMOOTH);
	#endif
    sntp_init();
}

static void obtain_time(void)
{
    initialize_sntp();

    // wait for time to be set
    time_t now = 0;
    struct tm timeinfo = { 0 };
    int retry = 0;
    const int retry_count = 10;
    while (sntp_get_sync_status() == SNTP_SYNC_STATUS_RESET && ++retry < retry_count) {
        //ESP_LOGI(TAG, "Waiting for system time to be set... (%d/%d)", retry, retry_count);
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
    time(&now);
    localtime_r(&now, &timeinfo);
}

int getStrength(int points){

	wifi_ap_record_t wifidata;
	long rssi = 0;
    long averageRSSI = 0;

    for (int i=0;i < points;i++){
    	if (esp_wifi_sta_get_ap_info(&wifidata)==FALSE){

    	rssi += wifidata.rssi;
    	vTaskDelay(25 / portTICK_PERIOD_MS);
    	}
    }

   averageRSSI=rssi/points;
   return averageRSSI;
}

esp_err_t pingResults(ping_target_id_t msgType, esp_ping_found * pf){
	ping.ping_max = pf->max_time;
	ping.ping_min = pf->min_time;
	ping.ping_mean = pf->total_time/pf->recv_count;
	ping.ping_payload = pf->bytes;
	ping.ping_tx_cnt = pf->send_count;
	ping.ping_rx_cnt = pf->recv_count;
	ping.ping_lost_cnt = pf->err_count;
	#ifdef Debug_Log
	printf("AvgTime:%.1fmS Sent:%d Rec:%d Err:%d min(mS):%d max(mS):%d ", (float)pf->total_time/pf->recv_count, pf->send_count, pf->recv_count, pf->err_count, pf->min_time, pf->max_time );
	printf("Resp(mS):%d Timeouts:%d Total Time:%d\n",pf->resp_time, pf->timeout_count, pf->total_time);
	#endif
	return ESP_OK;
}

static void power_init_callback(void* arg)
{
    if(RED_LED_STATUS == LOW){
    	RED_LED_TIMER_COUNT = RED_LED_BLINK_ON;
    	gpio_set_level(GPIO_OUTPUT_ESP_LED1_RED, HIGH);
    	RED_LED_STATUS = HIGH;
    }
    else{
    	RED_LED_TIMER_COUNT = RED_LED_BLINK_OFF;
    	gpio_set_level(GPIO_OUTPUT_ESP_LED1_RED, LOW);
    	RED_LED_STATUS = LOW;
    }
	ESP_ERROR_CHECK(esp_timer_stop(power_init_timer));
	ESP_ERROR_CHECK(esp_timer_start_periodic(power_init_timer, RED_LED_TIMER_COUNT));
}

static void wifi_led_blink_callback(void* arg)
{
    if(GREEN_LED_STATUS == LOW){
    	GREEN_LED_TIMER_COUNT = GREEN_LED_BLINK_ON;
    	gpio_set_level(GPIO_OUTPUT_ESP_LED3_GREEN, HIGH);
    	GREEN_LED_STATUS = HIGH;
    }
    else{
    	GREEN_LED_TIMER_COUNT = GREEN_LED_BLINK_OFF;
    	gpio_set_level(GPIO_OUTPUT_ESP_LED3_GREEN, LOW);
    	GREEN_LED_STATUS = LOW;
    }
}

static void wifi_led_short_callback(void* arg)
{
    if(GREEN_LED_STATUS == LOW){
    	GREEN_LED_TIMER_COUNT = GREEN_LED_SHORT_ON;
    	gpio_set_level(GPIO_OUTPUT_ESP_LED3_GREEN, HIGH);
    	GREEN_LED_STATUS = HIGH;
    }
    else{
    	GREEN_LED_TIMER_COUNT = GREEN_LED_SHORT_OFF;
    	gpio_set_level(GPIO_OUTPUT_ESP_LED3_GREEN, LOW);
    	GREEN_LED_STATUS = LOW;
    }
	ESP_ERROR_CHECK(esp_timer_stop(wifi_led_short_timer));
	ESP_ERROR_CHECK(esp_timer_start_periodic(wifi_led_short_timer, GREEN_LED_TIMER_COUNT));
}

static void fluid_led_blink_callback(void* arg)
{
    if(AMBER_LED_STATUS == LOW){
    	AMBER_LED_TIMER_COUNT = AMBER_LED_BLINK_ON;
    	gpio_set_level(GPIO_OUTPUT_ESP_LED2_AMBER, HIGH);
    	AMBER_LED_STATUS = HIGH;
    }
    else{
    	AMBER_LED_TIMER_COUNT = AMBER_LED_BLINK_OFF;
    	gpio_set_level(GPIO_OUTPUT_ESP_LED2_AMBER, LOW);
    	AMBER_LED_STATUS = LOW;
    }
}

static void fluid_dispense_motor_callback(void* arg){
	gpio_expander_write_byte(GPIO_EXPANDER_DEV_ADDR, EXPANDER_OUT_PORT_ADDR, EXPANDER_NULL_DATA);
	ESP_ERROR_CHECK(esp_timer_stop(fluid_led_blink_timer));
	gpio_set_level(GPIO_OUTPUT_ESP_LED2_AMBER, HIGH);
	if(SW_IoT_NonIoT_Status == IoT_MODE){
		ESP_ERROR_CHECK(esp_timer_stop(wifi_led_blink_timer));
	    gpio_set_level(GPIO_OUTPUT_ESP_LED3_GREEN, HIGH);
	}
	ESP_ERROR_CHECK(esp_timer_stop(fluid_dispense_motor_timer));
	#ifdef Debug_Log
	printf("MOTOR OFF...\n");
	#endif
	gpio_set_level(GPIO_OUTPUT_ESP_LED1_RED, LOW);
}

static void fluid_low_long_callback(void* arg)
{
    if(FLUID_LOW_STATUS == LOW){
    	gpio_expander_write_byte(GPIO_EXPANDER_DEV_ADDR, EXPANDER_OUT_PORT_ADDR, EXPANDER_BUZZER_DATA);
		#ifdef Debug_Log
    	printf("\nIF FLUID FULL OUT DATA:%d       Low_level_Buzzer_Count:%d\n",gpio_expander_read_byte(GPIO_EXPANDER_DEV_ADDR, EXPANDER_OUT_PORT_ADDR),Low_level_Buzzer_Count);
		#endif
    	vTaskDelay(50 / portTICK_RATE_MS);
    	FLUID_LOW_TIMER_COUNT = BUZZER_FLUID_LOW_LONG_ON;
    	gpio_set_level(GPIO_OUTPUT_ESP_LED2_AMBER, HIGH);
    	FLUID_LOW_STATUS = HIGH;
    	Low_level_Buzzer_Count++;
    }
    else{
    	gpio_expander_write_byte(GPIO_EXPANDER_DEV_ADDR, EXPANDER_OUT_PORT_ADDR, EXPANDER_NULL_DATA);
		#ifdef Debug_Log
    	printf("\nELSE FLUID FULL OUT DATA:%d\n",gpio_expander_read_byte(GPIO_EXPANDER_DEV_ADDR, EXPANDER_OUT_PORT_ADDR));
		#endif
    	vTaskDelay(50 / portTICK_RATE_MS);
    	FLUID_LOW_TIMER_COUNT = BUZZER_FLUID_LOW_LONG_OFF;
    	gpio_set_level(GPIO_OUTPUT_ESP_LED2_AMBER, LOW);
    	FLUID_LOW_STATUS = LOW;
    }
	ESP_ERROR_CHECK(esp_timer_stop(fluid_low_long_timer));
	ESP_ERROR_CHECK(esp_timer_start_periodic(fluid_low_long_timer, FLUID_LOW_TIMER_COUNT));
}

static void fluid_low_buzzer_delay_timer_callback(void* arg){
	if(fluid_low_buzzer_delay_timer_status == LOW){
		ESP_ERROR_CHECK(esp_timer_stop(fluid_low_buzzer_delay_timer));
		ESP_ERROR_CHECK(esp_timer_start_periodic(fluid_low_long_timer, FLUID_LOW_TIMER_COUNT));
	}
	else{
		ESP_ERROR_CHECK(esp_timer_stop(fluid_low_buzzer_delay_timer));
		fluid_low_buzzer_delay_timer_status = HIGH;
	}
}

static void fluid_full_long_callback(void* arg)
{
    if(FLUID_FULL_STATUS == LOW){
    	gpio_expander_write_byte(GPIO_EXPANDER_DEV_ADDR, EXPANDER_OUT_PORT_ADDR, EXPANDER_BUZZER_DATA);
		#ifdef Debug_Log
    	printf("\nIF FLUID FULL OUT DATA:%d\n",gpio_expander_read_byte(GPIO_EXPANDER_DEV_ADDR, EXPANDER_OUT_PORT_ADDR));
		#endif
    	vTaskDelay(50 / portTICK_RATE_MS);
    	FLUID_FULL_TIMER_COUNT = BUZZER_FLUID_FULL_ON;
    	FLUID_FULL_STATUS = HIGH;
    }
    else{
    	gpio_expander_write_byte(GPIO_EXPANDER_DEV_ADDR, EXPANDER_OUT_PORT_ADDR, EXPANDER_NULL_DATA);
		#ifdef Debug_Log
    	printf("\nELSE FLUID FULL OUT DATA:%d\n",gpio_expander_read_byte(GPIO_EXPANDER_DEV_ADDR, EXPANDER_OUT_PORT_ADDR));
		#endif
    	vTaskDelay(50 / portTICK_RATE_MS);
    	FLUID_FULL_TIMER_COUNT = BUZZER_FLUID_FULL_OFF;
    	FLUID_FULL_STATUS = LOW;
    }
    High_level_Buzzer_Count++;
	#ifdef Debug_Log
    printf("\nfluid count:%d..\n",High_level_Buzzer_Count);
	#endif
    ESP_ERROR_CHECK(esp_timer_stop(fluid_full_long_timer));
	ESP_ERROR_CHECK(esp_timer_start_periodic(fluid_full_long_timer, FLUID_FULL_TIMER_COUNT));
}

void create_message_header(char *msgbuf)
{
	if((header.pac_id == MQTT_DAILY_MAX_PAC_COUNT) || (header.DateTime.hour == 23 && header.DateTime.min == 59 && header.DateTime.sec == 59)){
		header.pac_id = 1;
		dispense.dispense_count = 0;
	}
	else{
		header.pac_id++;
	}
	sprintf(msgbuf,"%d,%.2x%.2x%.2x%.2x%.2x%.2x%.2x%.2x%.2x%.2x%.2x%.2x%.2x%.2x%.2x,%.2x,%d,%.2d%.2d%.2d",header.pac_id,
  										header.sta_id[0],header.sta_id[1],header.sta_id[2],header.sta_id[3],header.sta_id[4],header.sta_id[5],header.sta_id[6],header.sta_id[7],header.sta_id[8],header.sta_id[9],header.sta_id[10],header.sta_id[11],header.sta_id[12],header.sta_id[13],header.sta_id[14],
										header.msg_type, header.msg_length, header.DateTime.hour, header.DateTime.min, header.DateTime.sec);
}

void publish_system_health_status(){
	int qos_test = qos;
	char TxBuffer[80] = {"\0"};
	char msgbuf[MSG_HEADER_LEN] = {"\0"};
	create_message_header(msgbuf);
	sprintf(TxBuffer,"%s,%.2x,%.2x,%.2x,%.2x%.2x%.2x%.2x%.2x%.2x%.2x%.2x%.2x%.2x%.2x%.2x%.2x%.2x%.2x,%.2x,%.2x,%.2x",msgbuf,status.rssi,status.battery,status.power_status,
			header.sta_id[0],header.sta_id[1],header.sta_id[2],header.sta_id[3],header.sta_id[4],header.sta_id[5],header.sta_id[6],header.sta_id[7],header.sta_id[8],header.sta_id[9],header.sta_id[10],header.sta_id[11],header.sta_id[12],header.sta_id[13],header.sta_id[14],
			dispense.dispense_volume,status.liquid_type,status.rfid);
	#ifdef Debug_Log
	printf("\n system health status data=%s\n\n",TxBuffer);
	#endif
	esp_mqtt_client_publish(mqtt_client, Mqtt_Pub_Topic, TxBuffer, strlen(TxBuffer),qos_test , mqtt_retain);
//	esp_mqtt_client_publish(mqtt_client, "240AC4C10300", TxBuffer, strlen(TxBuffer),qos_test , mqtt_retain);
}

static void system_health_status_callback(void* arg)
{
    status.rssi = getStrength(RSSI_AVG_COUNT);
    status.battery = BATTERY_LOW;
    status.power_status = MAINS_POWERED;
    publish_system_health_status();
}

void publish_POST(void)
{
	int qos_test = qos;
	char TxBuffer[60] = {"\0"};
	char msgbuf[MSG_HEADER_LEN] = {"\0"};

	post.boot_reason = COLD_START;
	//need to remove according to new document
	post.heart_beat = 0x0A;
	post.battery_detect = BATTERY_NOT_PRESENT;
	header.msg_type = POST_MSG_TYPE;
	header.msg_length = POST_MSG_LEN;

	create_message_header(msgbuf);
	sprintf(TxBuffer,"%s,%d,%d,%.2d%.2d%.2d,%.2d%.2d%.2d",msgbuf,post.boot_reason,post.battery_detect,header.DateTime.hour,header.DateTime.min,header.DateTime.sec,header.DateTime.day,header.DateTime.month,header.DateTime.year);
	#ifdef Debug_Log
	printf("\npost data=%s\n\n",TxBuffer);
	#endif
	esp_mqtt_client_publish(mqtt_client, Mqtt_Pub_Topic, TxBuffer, strlen(TxBuffer),qos_test , mqtt_retain);
//	esp_mqtt_client_publish(mqtt_client, Mqtt_Pub_Topic"240AC4C10300", TxBuffer, strlen(TxBuffer),qos_test , mqtt_retain);
}

char GetDispenseVol()
{
	//dispense.dispense_volume = 0x0A;
	return dispense.dispense_volume;
}
void publish_fluid_dispense(void)
{
	int qos_test = qos;
	char TxBuffer[50] = {"\0"};
	char msgbuf[MSG_HEADER_LEN] = {"\0"};
	header.msg_type = FL_DISP_MSG_TYPE;
	header.msg_length = FL_DISP_MSG_LEN;
	create_message_header(msgbuf);
	sprintf(TxBuffer,"%s,%.2x,%d",msgbuf,GetDispenseVol(),dispense.dispense_count);
	#ifdef Debug_Log
	printf("\nFluid data=%s\n\n",TxBuffer);
	#endif
	esp_mqtt_client_publish(mqtt_client, Mqtt_Pub_Topic, TxBuffer, strlen(TxBuffer),qos_test , mqtt_retain);
//	esp_mqtt_client_publish(mqtt_client, "240AC4C10300", TxBuffer, strlen(TxBuffer), qos_test, mqtt_retain);
}

void publish_fluid_level(void)
{
	int qos_test = qos;
	char TxBuffer[100] = {"\0"};
	char msgbuf[MSG_HEADER_LEN] = {"\0"};

	header.msg_type = FL_LVL_MSG_TYPE;
	header.msg_length = FL_LVL_MSG_LEN;
	create_message_header(msgbuf);
	sprintf(TxBuffer,"%s,%.2x",msgbuf,FluidLevel.level);
	#ifdef Debug_Log
	printf("\nPublish Fluid data=%s\n\n",TxBuffer);
	#endif
	esp_mqtt_client_publish(mqtt_client, Mqtt_Pub_Topic, TxBuffer, strlen(TxBuffer),qos_test , mqtt_retain);
//	esp_mqtt_client_publish(mqtt_client, "240AC4C10300", TxBuffer, strlen(TxBuffer), qos_test, mqtt_retain);
}

void publish_config_ack(void){
	int qos_test = qos;
	char TxBuffer[100] = {"\0"};
	char msgbuf[MSG_HEADER_LEN] = {"\0"};
	uint8_t msg_length = CONFIG_ACK_MSG_LEN;

	create_message_header(msgbuf);
	sprintf(TxBuffer,"%s,%.2x,%.2x,%.2x,%.2x,%.2d%.2d%.2d,%.2d%.2d%.2d",msgbuf,dispense.dispense_volume,config.version,config.msg_type,msg_length,header.DateTime.hour,header.DateTime.min,header.DateTime.sec,header.DateTime.day,header.DateTime.month,header.DateTime.year);
	#ifdef Debug_Log
	printf("\nConfig ACK data=%s\n\n",TxBuffer);
	#endif
	esp_mqtt_client_publish(mqtt_client, Mqtt_Pub_Topic, TxBuffer, strlen(TxBuffer),qos_test , mqtt_retain);
//	esp_mqtt_client_publish(mqtt_client, "240AC4C10300", TxBuffer, strlen(TxBuffer), qos_test, mqtt_retain);
}

static void FD_Out(void* arg)
{
    uint32_t io_num;
    for(;;) {
        if(xQueueReceive(gpio_evt_queue, &io_num, portMAX_DELAY)) {
//        	printf("GPIO[%d] intr, val: %d ", io_num, gpio_get_level(io_num));
//        	if((fluid_dispence_status == 0) && (io_num == GPIO_INPUT_P_IR) && (gpio_get_level(GPIO_INPUT_P_IR) == 0)){
        	if(io_num == GPIO_INPUT_P_IR){
        		if((fluid_dispence_status == FALSE) && (gpio_get_level(GPIO_INPUT_P_IR) == LOW)){
					#ifdef Debug_Log
        			printf("\n....p_ir high....%.2d%.2d%.2d\n",header.DateTime.hour,header.DateTime.min,header.DateTime.sec);
					#endif
        			gpio_set_level(GPIO_OUTPUT_ESP_LED1_RED, HIGH);
					fluid_dispence_status = TRUE;
					AMBER_LED_TIMER_COUNT = AMBER_LED_BLINK_OFF;
					ESP_ERROR_CHECK(esp_timer_start_periodic(fluid_led_blink_timer, AMBER_LED_TIMER_COUNT));
					if(SW_IoT_NonIoT_Status == IoT_MODE){
						GREEN_LED_TIMER_COUNT = GREEN_LED_BLINK_OFF;
						ESP_ERROR_CHECK(esp_timer_start_periodic(wifi_led_blink_timer, GREEN_LED_TIMER_COUNT));
					}
					dispense.dispense_count++;
					#ifdef Debug_Log
					printf("disp_count=%d\n",dispense.dispense_count);
					#endif
					gpio_expander_write_byte(GPIO_EXPANDER_DEV_ADDR, EXPANDER_OUT_PORT_ADDR, EXPANDER_MOTOR_DATA);
					#ifdef Debug_Log
					printf("\nMOTROR OUT DATA:%d\n",gpio_expander_read_byte(GPIO_EXPANDER_DEV_ADDR, EXPANDER_OUT_PORT_ADDR));
					printf("MOTOR ON...\n");
					#endif
					ESP_ERROR_CHECK(esp_timer_start_periodic(fluid_dispense_motor_timer, CONF_DELAY));
					if(SW_IoT_NonIoT_Status == IoT_MODE){
						publish_fluid_dispense();
					}
        		}
        		else if((fluid_dispence_status == TRUE) && (gpio_get_level(GPIO_INPUT_P_IR) == HIGH)){
        			vTaskDelay(2000 / portTICK_RATE_MS);
        			fluid_dispence_status = FALSE;
					#ifdef Debug_Log
        			printf("\n....p_ir high....%.2d%.2d%.2d\n",header.DateTime.hour,header.DateTime.min,header.DateTime.sec);
					#endif
        		}
        		else{
					#ifdef Debug_Log
        			printf("\n...P_IR INT...\n");
					#endif
        		}
        	}
        	else if(io_num == GPIO_INPUT_LL_IR){
				#ifdef Debug_Log
        		printf(".\n....GPIO_INPUT_LL_IR=%d\n",gpio_get_level(GPIO_INPUT_LL_IR));
				#endif
        		if(gpio_get_level(GPIO_INPUT_LL_IR) == HIGH){
        			fluid_low_buzzer_delay_timer_status = FALSE;
					FLUID_LOW_TIMER_COUNT = BUZZER_FLUID_LOW_LONG_OFF;
					ESP_ERROR_CHECK(esp_timer_start_periodic(fluid_low_long_timer, FLUID_LOW_TIMER_COUNT));
					#ifdef Debug_Log
					printf("Fluid Amber LED...\n");
					#endif
					if(SW_IoT_NonIoT_Status == IoT_MODE){
						GREEN_LED_TIMER_COUNT = GREEN_LED_BLINK_OFF;
						ESP_ERROR_CHECK(esp_timer_start_periodic(wifi_led_blink_timer, GREEN_LED_TIMER_COUNT));
						FluidLevel.level = FLUID_LEVEL_LOW;
						publish_fluid_level();
						ESP_ERROR_CHECK(esp_timer_stop(wifi_led_blink_timer));
						gpio_set_level(GPIO_OUTPUT_ESP_LED3_GREEN, HIGH);
					}
					gpio_set_level(GPIO_OUTPUT_ESP_LED2_AMBER, HIGH);
        		}
        		else if(gpio_get_level(GPIO_INPUT_LL_IR) == LOW){
					#ifdef Debug_Log
        			printf("\n...else...");
					#endif
        			fluid_low_buzzer_delay_timer_status = TRUE;
        			gpio_expander_write_byte(GPIO_EXPANDER_DEV_ADDR, EXPANDER_OUT_PORT_ADDR, EXPANDER_NULL_DATA);
        			gpio_set_level(GPIO_OUTPUT_ESP_LED2_AMBER, HIGH);
        	    	if(SW_IoT_NonIoT_Status == IoT_MODE){
        				GREEN_LED_TIMER_COUNT = GREEN_LED_BLINK_OFF;
        				ESP_ERROR_CHECK(esp_timer_start_periodic(wifi_led_blink_timer, GREEN_LED_TIMER_COUNT));
        	    		FluidLevel.level = FLUID_LEVEL_BELOW_CENTER;
        	    		publish_fluid_level();
        				ESP_ERROR_CHECK(esp_timer_stop(wifi_led_blink_timer));
        				gpio_set_level(GPIO_OUTPUT_ESP_LED3_GREEN, HIGH);
        	    	}
        		}
        	}
        	else if(io_num == GPIO_INPUT_LF_IR){
        	    if(gpio_get_level(GPIO_INPUT_LF_IR) == LOW){
					FLUID_FULL_TIMER_COUNT = BUZZER_FLUID_FULL_OFF;
					ESP_ERROR_CHECK(esp_timer_start_periodic(fluid_full_long_timer, FLUID_FULL_TIMER_COUNT));
					#ifdef Debug_Log
					printf("Fluid Full Buzzer...\n");
					#endif
					if(SW_IoT_NonIoT_Status == IoT_MODE){
						GREEN_LED_TIMER_COUNT = GREEN_LED_BLINK_OFF;
						ESP_ERROR_CHECK(esp_timer_start_periodic(wifi_led_blink_timer, GREEN_LED_TIMER_COUNT));
						FluidLevel.level = FLUID_LEVEL_HIGH;
						publish_fluid_level();
						ESP_ERROR_CHECK(esp_timer_stop(wifi_led_blink_timer));
						gpio_set_level(GPIO_OUTPUT_ESP_LED3_GREEN, HIGH);
					}
        	    }
        	    else if(gpio_get_level(GPIO_INPUT_LF_IR) == HIGH){
        	    	if(SW_IoT_NonIoT_Status == IoT_MODE){
        				GREEN_LED_TIMER_COUNT = GREEN_LED_BLINK_OFF;
        				ESP_ERROR_CHECK(esp_timer_start_periodic(wifi_led_blink_timer, GREEN_LED_TIMER_COUNT));
        	    		FluidLevel.level = FLUID_LEVEL_ABOVE_CENTER;
        	    		publish_fluid_level();
        				ESP_ERROR_CHECK(esp_timer_stop(wifi_led_blink_timer));
        				gpio_set_level(GPIO_OUTPUT_ESP_LED3_GREEN, HIGH);
        	    	}
        	    }
        	}
        	else if((io_num == GPIO_EXPANDER_INT) && (gpio_get_level(GPIO_EXPANDER_INT) == LOW)){
        		SW_IoT_NonIoT_Status = gpio_expander_read_byte(GPIO_EXPANDER_DEV_ADDR, EXPANDER_IN_PORT_ADDR);
				#ifdef Debug_Log
        		printf("SW_IOT_NONIOT_STATUS=%d\n",SW_IoT_NonIoT_Status);
				#endif
        	}
        	else if(io_num == GPIO_INPUT_LOW_BATT){
				#ifdef Debug_Log
        		printf("Power Red LED...\n");
				#endif
        	}
        }
    }

}

static esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event)
{
    esp_mqtt_client_handle_t client = event->client;
	#ifdef Debug_Log
    static int msg_id = 0;
	#endif
    char SubMsg[50] = {"\0"};
	#ifdef Debug_Log
    printf("event->id=%d,\n",event->event_id);
	#endif
    switch (event->event_id) {
    case MQTT_EVENT_CONNECTED:
	#ifdef Debug_Log
    	ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
	#endif
        xEventGroupSetBits(mqtt_event_group, CONNECTED_BIT);
        esp_mqtt_client_subscribe(client, Mqtt_Sub_Topic, qos);
//        msg_id = esp_mqtt_client_subscribe(client, Mqtt_Sub_Topic, qos);
//        msg_id = esp_mqtt_client_subscribe(client, "bcddc2de772c", qos);
	#ifdef Debug_Log
        printf("msg_id %d\n",msg_id);
        ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);
	#endif
        break;
    case MQTT_EVENT_DISCONNECTED:
	#ifdef Debug_Log
    	ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
	#endif
    	break;

    case MQTT_EVENT_SUBSCRIBED:
	#ifdef Debug_Log
    	ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
	#endif
    	break;
    case MQTT_EVENT_UNSUBSCRIBED:
	#ifdef Debug_Log
    	ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
	#endif
    	break;
    case MQTT_EVENT_PUBLISHED:
	#ifdef Debug_Log
    	ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
	#endif
    	break;
    case MQTT_EVENT_DATA:
	#ifdef Debug_Log
    	ESP_LOGI(TAG, "MQTT_EVENT_DATA");
        ESP_LOGI(TAG, "[APP] before Free memory: %d bytes", esp_get_free_heap_size());
        printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
        printf("DATA=%.*s\r\n", event->data_len, event->data);
        printf("ID=%d, total_len=%d, data_len=%d, current_data_offset=%d\n", event->msg_id, event->total_data_len, event->data_len, event->current_data_offset);
	#endif
        strncpy(SubMsg,event->data,event->data_len);
        sscanf(SubMsg,"%x,%x,%x,%s,%x,%x,%s", &config.dispense_volume,&config.rfid_flag,&config.version,config.liquid_type,&config.msg_type,&config.msg_length,config.time);
        dispense.dispense_volume = config.dispense_volume;
        conf_fluid_dispense_ack = TRUE;
        break;
    case MQTT_EVENT_ERROR:
	#ifdef Debug_Log
    	ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
	#endif
    	break;
    default:
	#ifdef Debug_Log
    	ESP_LOGI(TAG, "Other event id:%d", event->event_id);
	#endif
    	break;
    }
    return ESP_OK;
}

static void mqtt_app_start(void)
{
    mqtt_event_group = xEventGroupCreate();
    const esp_mqtt_client_config_t mqtt_cfg = {
        .event_handle = mqtt_event_handler,
        .cert_pem = (const char *)mqtt_eclipse_org_pem_start,
		.uri	=	Mqtt_L9_ServerURL,
		//.uri	= "mqtt://3.7.202.112:1883",
    };
	#ifdef Debug_Log
    ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
	#endif
    mqtt_client = esp_mqtt_client_init(&mqtt_cfg);
}

void app_main()
{
	gpio_pad_select_gpio(GPIO_EXPANDER_RESET_ENABLE);
	gpio_set_direction(GPIO_EXPANDER_RESET_ENABLE, GPIO_MODE_OUTPUT);
	gpio_set_level(GPIO_EXPANDER_RESET_ENABLE, HIGH);
	SW_IoT_NonIoT_Status = IoT_MODE;
	init_i2c_master();

	esp_rom_spiflash_config_param(DEVICE_ID, FLASH_SIZE, FLASH_BLOCK_SIZE, FLASH_SECTOR_SIZE, FLASH_PAGE_SIZE, STATUS_MASK);

	gpio_config_t io_conf;
	io_conf.mode = GPIO_MODE_OUTPUT;
//    io_conf.pull_up_en = 1;
//    io_conf.pull_down_en = 1;
	io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL;
	gpio_config(&io_conf);

	gpio_set_level(GPIO_OUTPUT_ESP_LED1_RED, LOW);
	gpio_set_level(GPIO_OUTPUT_ESP_LED2_AMBER, LOW);
	gpio_set_level(GPIO_OUTPUT_ESP_LED3_GREEN, LOW);

	RED_LED_STATUS = LOW;
	AMBER_LED_STATUS = LOW;
	GREEN_LED_STATUS = LOW;

    //interrupt of rising edge
    io_conf.intr_type = GPIO_PIN_INTR_POSEDGE;
    //io_conf.intr_type = GPIO_INTR_NEGEDGE;GPIO_INTR_ANYEDGE
    //io_conf.intr_type = GPIO_INTR_LOW_LEVEL;
    io_conf.pin_bit_mask = GPIO_INPUT_PIN_SEL;
    //set as input mode
    io_conf.mode = GPIO_MODE_INPUT;
    //enable pull-up mode
    io_conf.pull_up_en = HIGH;
    //io_conf.pull_down_en = HIGH;
    gpio_config(&io_conf);

    gpio_set_intr_type(GPIO_INPUT_P_IR, GPIO_INTR_ANYEDGE);
    gpio_set_intr_type(GPIO_INPUT_LL_IR, GPIO_INTR_ANYEDGE);
    gpio_set_intr_type(GPIO_INPUT_LF_IR, GPIO_INTR_ANYEDGE);
    gpio_set_intr_type(GPIO_EXPANDER_INT, GPIO_INTR_NEGEDGE);

    //create a queue to handle gpio event from isr
    gpio_evt_queue = xQueueCreate(GPIO_INT_QUUE_SIZE, sizeof(uint32_t));
    //start gpio task
    xTaskCreate(FD_Out, "FD_Out", GPIO_INT_STACK_DEPTH, NULL, GPIO_INT_TASK_PRIORITY, NULL);

    const esp_timer_create_args_t power_init = {
            .callback = &power_init_callback,
            /* name is optional, but may help identify the timer when debugging */
            .name = "power_init"
    };
    /* The timer has been created but is not running yet */
    ESP_ERROR_CHECK(esp_timer_create(&power_init, &power_init_timer));
    RED_LED_TIMER_COUNT = RED_LED_BLINK_OFF;
    /* Start the timers */
    ESP_ERROR_CHECK(esp_timer_start_periodic(power_init_timer, RED_LED_TIMER_COUNT));

    //hook isr handler for specific gpio pin
    gpio_isr_handler_add(GPIO_EXPANDER_INT, gpio_isr_handler, (void*) GPIO_EXPANDER_INT);

	        const esp_timer_create_args_t fluid_led_blink = {
	                .callback = &fluid_led_blink_callback,
	                /* name is optional, but may help identify the timer when debugging */
	                .name = "fluid_led_blink"
	        };
	        ESP_ERROR_CHECK(esp_timer_create(&fluid_led_blink, &fluid_led_blink_timer));

	        const esp_timer_create_args_t fluid_low_long = {
	                .callback = &fluid_low_long_callback,
	                /* name is optional, but may help identify the timer when debugging */
	                .name = "fluid_low_long"
	        };
	        ESP_ERROR_CHECK(esp_timer_create(&fluid_low_long, &fluid_low_long_timer));

	        const esp_timer_create_args_t fluid_full_long = {
	                .callback = &fluid_full_long_callback,
	                /* name is optional, but may help identify the timer when debugging */
	                .name = "fluid_full_long"
	        };
	        ESP_ERROR_CHECK(esp_timer_create(&fluid_full_long, &fluid_full_long_timer));

	        const esp_timer_create_args_t fluid_dispense_motor = {
	                .callback = &fluid_dispense_motor_callback,
	                /* name is optional, but may help identify the timer when debugging */
	                .name = "fluid_dispense_motor"
	        };
	        ESP_ERROR_CHECK(esp_timer_create(&fluid_dispense_motor, &fluid_dispense_motor_timer));

	        const esp_timer_create_args_t fluid_low_buzzer_delay = {
	                .callback = &fluid_low_buzzer_delay_timer_callback,
	                /* name is optional, but may help identify the timer when debugging */
	                .name = "fluid_low_buzzer_delay"
	        };
	        ESP_ERROR_CHECK(esp_timer_create(&fluid_low_buzzer_delay, &fluid_low_buzzer_delay_timer));

	time_t now;
	struct tm timeinfo;
	struct timeval outdelta;

	if(SW_IoT_NonIoT_Status == IoT_MODE){
		esp_log_level_set("*", ESP_LOG_INFO);
		esp_log_level_set("MQTT_CLIENT", ESP_LOG_VERBOSE);
		esp_log_level_set("TRANSPORT_TCP", ESP_LOG_VERBOSE);
		esp_log_level_set("TRANSPORT_SSL", ESP_LOG_VERBOSE);
		esp_log_level_set("TRANSPORT", ESP_LOG_VERBOSE);
		esp_log_level_set("OUTBOX", ESP_LOG_VERBOSE);

			const esp_timer_create_args_t wifi_led_short = {
					.callback = &wifi_led_short_callback,
					/* name is optional, but may help identify the timer when debugging */
					.name = "wifi_led_short"
			};
			/* The timer has been created but is not running yet */
			ESP_ERROR_CHECK(esp_timer_create(&wifi_led_short, &wifi_led_short_timer));
			GREEN_LED_TIMER_COUNT = GREEN_LED_SHORT_OFF;
			/* Start the timers */
			ESP_ERROR_CHECK(esp_timer_start_periodic(wifi_led_short_timer, GREEN_LED_TIMER_COUNT));

			const esp_timer_create_args_t wifi_led_blink = {
					.callback = &wifi_led_blink_callback,
					/* name is optional, but may help identify the timer when debugging */
					.name = "wifi_led_blink"
			};
			ESP_ERROR_CHECK(esp_timer_create(&wifi_led_blink, &wifi_led_blink_timer));

		//Initialize NVS
		esp_err_t ret = nvs_flash_init();
		if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
		  ESP_ERROR_CHECK(nvs_flash_erase());
		  ret = nvs_flash_init();
		}
		ESP_ERROR_CHECK(ret);

		vTaskDelay(1000 / portTICK_RATE_MS);
		#ifdef Debug_Log
		ESP_LOGI(TAG, "ESP_WIFI_MODE_STA");
		#endif
		initialise_wifi();
	}

	if(SW_IoT_NonIoT_Status == IoT_MODE){
		vTaskDelay(1000/portTICK_PERIOD_MS);
		time(&now);
		localtime_r(&now, &timeinfo);

		// Is time set? If not, tm_year will be (1970 - 1900).
		if (timeinfo.tm_year < (2016 - 1900)) {
			#ifdef Debug_Log
			ESP_LOGI(TAG, "Time is not set yet. Connecting to WiFi and getting time over NTP.");
			#endif
			obtain_time();
			// update 'now' variable with current time
			time(&now);
		}
	#ifdef CONFIG_SNTP_TIME_SYNC_METHOD_SMOOTH
		else {
			// add 500 ms error to the current system time.
			// Only to demonstrate a work of adjusting method!
			{
				#ifdef Debug_Log
				ESP_LOGI(TAG, "Add a error for test adjtime");
				#endif
				struct timeval tv_now;
				gettimeofday(&tv_now, NULL);
				int64_t cpu_time = (int64_t)tv_now.tv_sec * 1000000L + (int64_t)tv_now.tv_usec;
				int64_t error_time = cpu_time + 500 * 1000L;
				struct timeval tv_error = { .tv_sec = error_time / 1000000L, .tv_usec = error_time % 1000000L };
				settimeofday(&tv_error, NULL);
			}
			#ifdef Debug_Log
			ESP_LOGI(TAG, "Time was set, now just adjusting it. Use SMOOTH SYNC method.");
			#endif
			obtain_time();
			// update 'now' variable with current time
			time(&now);
		}
	#endif
		// Set timezone to India Standard Time
		setenv("TZ", "UTC-05:30", 1);
		tzset();
		localtime_r(&now, &timeinfo);
		header.DateTime.hour = timeinfo.tm_hour;
		header.DateTime.min = timeinfo.tm_min;
		header.DateTime.sec = timeinfo.tm_sec;
		header.DateTime.day = timeinfo.tm_mday;
		header.DateTime.month = timeinfo.tm_mon+1;
		header.DateTime.year = timeinfo.tm_year+1900;
		if (sntp_get_sync_mode() == SNTP_SYNC_MODE_SMOOTH) {
			while (sntp_get_sync_status() == SNTP_SYNC_STATUS_IN_PROGRESS) {
				adjtime(NULL, &outdelta);
			   /* ESP_LOGI(TAG, "Waiting for adjusting time ... outdelta = %li sec: %li ms: %li us",
							outdelta.tv_sec,
							outdelta.tv_usec/1000,
							outdelta.tv_usec%1000);*/
				vTaskDelay(3000 / portTICK_PERIOD_MS);
			}
		}

		const esp_timer_create_args_t system_health_status = {
            .callback = &system_health_status_callback,
            /* name is optional, but may help identify the timer when debugging */
            .name = "system_health_status"
		};

		esp_timer_handle_t system_health_status_timer;
		/* The timer has been created but is not running yet */
		ESP_ERROR_CHECK(esp_timer_create(&system_health_status, &system_health_status_timer));
	    /* Start the timers */
	    ESP_ERROR_CHECK(esp_timer_start_periodic(system_health_status_timer, SYSTEM_HEALTH_STATUS_TIME));

		mqtt_app_start();
		esp_mqtt_client_set_uri(mqtt_client, Mqtt_L9_ServerURL);
//		esp_mqtt_client_set_uri(mqtt_client, Mqtt_L9_ServerURL"mqtt://3.7.202.112:1883");
		esp_mqtt_client_start(mqtt_client);
		xEventGroupClearBits(mqtt_event_group, CONNECTED_BIT);
		xEventGroupWaitBits(mqtt_event_group, CONNECTED_BIT, false, true, portMAX_DELAY);

		#ifdef	Debug_Log
		uint8_t PingCnt = 0;
		while(PingCnt != PING_COUNT){
			gw.addr = ipaddr_addr("3.7.202.112");
			esp_ping_set_target(PING_TARGET_IP_ADDRESS_COUNT, &ping_count, sizeof(uint32_t));
			esp_ping_set_target(PING_TARGET_RCV_TIMEO, &ping_timeout, sizeof(uint32_t));
			esp_ping_set_target(PING_TARGET_DELAY_TIME, &ping_delay, sizeof(uint32_t));
			esp_ping_set_target(PING_TARGET_IP_ADDRESS, &gw.addr, sizeof(uint32_t));
			esp_ping_set_target(PING_TARGET_RES_FN, &pingResults, sizeof(pingResults));
			printf("\nPinging IP %s WiFi rssi %d\n",inet_ntoa(gw),getStrength(RSSI_AVG_COUNT));
			printf("%s, %d, ",inet_ntoa(gw),getStrength(RSSI_AVG_COUNT));
			PingCnt++;
		}
		#endif
		ESP_ERROR_CHECK(esp_timer_stop(wifi_led_short_timer));
		ESP_ERROR_CHECK(esp_timer_delete(wifi_led_short_timer));
		gpio_set_level(GPIO_OUTPUT_ESP_LED3_GREEN, HIGH);
		publish_POST();
    }
	else{//executed during non_iot mode
		ESP_ERROR_CHECK(esp_timer_stop(wifi_led_short_timer));
		ESP_ERROR_CHECK(esp_timer_delete(wifi_led_short_timer));
		gpio_set_level(GPIO_OUTPUT_ESP_LED3_GREEN, LOW);
	}

    //install gpio isr service
    gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
    //hook isr handler for specific gpio pin
    gpio_isr_handler_add(GPIO_INPUT_P_IR, gpio_isr_handler, (void*) GPIO_INPUT_P_IR);
    //hook isr handler for specific gpio pin
    gpio_isr_handler_add(GPIO_INPUT_LL_IR, gpio_isr_handler, (void*) GPIO_INPUT_LL_IR);
    //hook isr handler for specific gpio pin
    gpio_isr_handler_add(GPIO_INPUT_LF_IR, gpio_isr_handler, (void*) GPIO_INPUT_LF_IR);
    //hook isr handler for specific gpio pin
    //gpio_isr_handler_add(GPIO_INPUT_LOW_BATT, gpio_isr_handler, (void*) GPIO_INPUT_LOW_BATT);

	dispense.dispense_volume = DEFAULT_DISPENSE_VOLUME;

    ESP_ERROR_CHECK(esp_timer_stop(power_init_timer));
    ESP_ERROR_CHECK(esp_timer_delete(power_init_timer));
//    gpio_set_level(GPIO_OUTPUT_ESP_LED1_RED, HIGH);
    gpio_set_level(GPIO_OUTPUT_ESP_LED1_RED, LOW);
    gpio_set_level(GPIO_OUTPUT_ESP_LED2_AMBER, HIGH);
    gpio_expander_write_byte(GPIO_EXPANDER_DEV_ADDR, EXPANDER_OUT_PORT_ADDR, EXPANDER_BUZZER_DATA);
    vTaskDelay(200 / portTICK_RATE_MS);
    gpio_expander_write_byte(GPIO_EXPANDER_DEV_ADDR, EXPANDER_OUT_PORT_ADDR, EXPANDER_NULL_DATA);

    /*
     * if liquied available the senser sence low signal
     * else senser sence high
     * liquied level <10% pub 0x0a
     * liquied level >10% & <90% pub 0x32
     * liquied level >90% pub 0x64
     */
    if(gpio_get_level(GPIO_INPUT_LL_IR) == HIGH){
		fluid_low_buzzer_delay_timer_status = LOW;
		FLUID_LOW_TIMER_COUNT = BUZZER_FLUID_LOW_LONG_OFF;
		ESP_ERROR_CHECK(esp_timer_start_periodic(fluid_low_long_timer, FLUID_LOW_TIMER_COUNT));
		#ifdef Debug_Log
		printf("Fluid Amber LED...\n");
		#endif
		if(SW_IoT_NonIoT_Status == IoT_MODE){
			GREEN_LED_TIMER_COUNT = GREEN_LED_BLINK_OFF;
			ESP_ERROR_CHECK(esp_timer_start_periodic(wifi_led_blink_timer, GREEN_LED_TIMER_COUNT));
			FluidLevel.level = FLUID_LEVEL_LOW;
			publish_fluid_level();
			ESP_ERROR_CHECK(esp_timer_stop(wifi_led_blink_timer));
			gpio_set_level(GPIO_OUTPUT_ESP_LED3_GREEN, HIGH);
		}
		gpio_set_level(GPIO_OUTPUT_ESP_LED2_AMBER, HIGH);
    }
    else if(gpio_get_level(GPIO_INPUT_LL_IR) == LOW && gpio_get_level(GPIO_INPUT_LF_IR) == HIGH){
    	if(SW_IoT_NonIoT_Status == IoT_MODE){
			GREEN_LED_TIMER_COUNT = GREEN_LED_BLINK_OFF;
			ESP_ERROR_CHECK(esp_timer_start_periodic(wifi_led_blink_timer, GREEN_LED_TIMER_COUNT));
    		FluidLevel.level = FLUID_LEVEL_CENTER;
    		publish_fluid_level();
			ESP_ERROR_CHECK(esp_timer_stop(wifi_led_blink_timer));
			gpio_set_level(GPIO_OUTPUT_ESP_LED3_GREEN, HIGH);
    	}
    }
    else if(gpio_get_level(GPIO_INPUT_LF_IR) == LOW){
	    FLUID_FULL_TIMER_COUNT = BUZZER_FLUID_FULL_OFF;
	    ESP_ERROR_CHECK(esp_timer_start_periodic(fluid_full_long_timer, FLUID_FULL_TIMER_COUNT));
		#ifdef Debug_Log
	    printf("Fluid Full Buzzer...\n");
		#endif
	    if(SW_IoT_NonIoT_Status == IoT_MODE){
			GREEN_LED_TIMER_COUNT = GREEN_LED_BLINK_OFF;
			ESP_ERROR_CHECK(esp_timer_start_periodic(wifi_led_blink_timer, GREEN_LED_TIMER_COUNT));
			FluidLevel.level = FLUID_LEVEL_HIGH;
			publish_fluid_level();
			ESP_ERROR_CHECK(esp_timer_stop(wifi_led_blink_timer));
			gpio_set_level(GPIO_OUTPUT_ESP_LED3_GREEN, HIGH);
	    }
    }

    	/*esp_err_t ret;
        char* page_write_data = "SynologySmartConnect\0";
        ret = eeprom_write(0x50, 0x0000, (uint8_t*)page_write_data, strlen(page_write_data));
        if (ret == ESP_OK) printf("The write operation was successful!\n");

        vTaskDelay(50/portTICK_PERIOD_MS);

        uint8_t* sequential_read_data = (uint8_t*) malloc(strlen(page_write_data)+1);
        ret = eeprom_read(0x50, 0x0000, sequential_read_data, strlen(page_write_data)+1);

        if (ret == ESP_ERR_TIMEOUT) printf("I2C timeout...\n");
        if (ret == ESP_OK) printf("The read operation was successful!\n");
        else printf("The read operation was not successful, no ACK recieved.  Is the device connected properly?\n");

        printf("Read the following string from EEPROM: %s\n", (char*)sequential_read_data);

        free(sequential_read_data);*/

    while(1) {
    	if(SW_IoT_NonIoT_Status == NON_IoT_MODE){
    		gpio_set_level(GPIO_OUTPUT_ESP_LED3_GREEN, LOW);
    	}
    	if(SW_IoT_NonIoT_Status == IoT_MODE){
			time(&now);
			setenv("TZ", "UTC-05:30", 1);
			tzset();
			localtime_r(&now, &timeinfo);
			gettimeofday(&outdelta, NULL);
			header.DateTime.hour = timeinfo.tm_hour;
			header.DateTime.min = timeinfo.tm_min;
			header.DateTime.sec = timeinfo.tm_sec;
			header.DateTime.day = timeinfo.tm_mday;
			header.DateTime.month = timeinfo.tm_mon+1;
			header.DateTime.year = timeinfo.tm_year+1900;

			if(header.DateTime.hour==23 && header.DateTime.min==59 && header.DateTime.sec==59){
				header.pac_id = 0;
				dispense.dispense_count = 0;
			}
    	}
    	if(High_level_Buzzer_Count >= HIGH_LEVEL_DETECT_BUZZER_COUNT){
			#ifdef Debug_Log
    		printf("\nmain     High_level_Buzzer_Count=%d.........\n\n",High_level_Buzzer_Count);
			#endif
    		gpio_expander_write_byte(GPIO_EXPANDER_DEV_ADDR, EXPANDER_OUT_PORT_ADDR, EXPANDER_NULL_DATA);
    		ESP_ERROR_CHECK(esp_timer_stop(fluid_full_long_timer));
    		High_level_Buzzer_Count = FALSE;
    	}
    	if(Low_level_Buzzer_Count >= LOW_LEVEL_DETECT_BUZZER_COUNT){
    		gpio_expander_write_byte(GPIO_EXPANDER_DEV_ADDR, EXPANDER_OUT_PORT_ADDR, EXPANDER_NULL_DATA);
    		Low_level_Buzzer_Count = FALSE;
    		gpio_set_level(GPIO_OUTPUT_ESP_LED2_AMBER, HIGH);
    		ESP_ERROR_CHECK(esp_timer_stop(fluid_low_long_timer));
    		ESP_ERROR_CHECK(esp_timer_start_periodic(fluid_low_buzzer_delay_timer, FLUID_LOW_BUZZER_DELAY));
    	}
    	if(conf_fluid_dispense_ack == HIGH){
    		publish_config_ack();
    		CONF_DELAY = dispense.dispense_volume * 100000;
    		conf_fluid_dispense_ack = 0;
    	}
//    	publish_fluid_dispense();
//    	for (int i = 5; i >= 0; i--) {
//    	        printf("Restarting in %d seconds...\n", i);
//    	        vTaskDelay(1000 / portTICK_PERIOD_MS);
//    	    }
//    	    printf("Restarting now.\n");
//    	    fflush(stdout);
//    	    esp_restart();

    	vTaskDelay(1000 / portTICK_RATE_MS);
    	//printf("main...");
    }
}
